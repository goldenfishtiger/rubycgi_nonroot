FROM fedora

LABEL maintainer="Cnes Taro <goldenfishtiger@gmail.com>"

ARG TZ
ARG http_proxy
ARG https_proxy

RUN set -x \
	&& dnf install -y \
		httpd \
		ruby \
		procps-ng \
		net-tools \
		diffutils \
	&& rm -rf /var/cache/dnf/* \
	&& dnf clean all

# git clone http://gitlab.com/goldenfishtiger/rubycgi_nonroot しておくこと
COPY rubycgi_nonroot /var/www/cgi-bin/
RUN cp /var/www/cgi-bin/dot.htaccess /var/www/cgi-bin/.htaccess

RUN set -x \
	&& useradd user1 \
	&& rm /etc/localtime \
	&& cp /usr/share/zoneinfo/Etc/UTC /etc/localtime \
	&& chmod 666 /etc/localtime
RUN set -x \
	&& mv /etc/httpd/conf/httpd.conf /etc/httpd/conf/httpd.conf.org \
	&& cat /etc/httpd/conf/httpd.conf.org \
		| sed 's/^Listen 80/Listen 8080/' \
		| sed 's/^User apache/User user1/' \
		| sed 's/^Group apache/Group user1/' \
		| sed '/^<Directory "\/var\/www\/cgi-bin">/,/^</s/AllowOverride None/AllowOverride All/' \
		| sed '/^<Directory "\/var\/www\/cgi-bin">/,/^</s/Options None/Options All/' \
		> /etc/httpd/conf/httpd.conf \
	&& diff -C 2 /etc/httpd/conf/httpd.conf.org /etc/httpd/conf/httpd.conf \
	|| echo '/etc/httpd/conf/httpd.conf changed.'
RUN set -x \
	&& mv /usr/lib/tmpfiles.d/httpd.conf /usr/lib/tmpfiles.d/httpd.conf.org \
	&& cat /usr/lib/tmpfiles.d/httpd.conf.org \
		| sed 's/0 .* apache/0 user1 user1/' \
		> /usr/lib/tmpfiles.d/httpd.conf \
	&& diff -C 2 /usr/lib/tmpfiles.d/httpd.conf.org /usr/lib/tmpfiles.d/httpd.conf \
	|| echo '/usr/lib/tmpfiles.d/httpd.conf changed.'
RUN set -x \
	&& chown -R user1:user1 /var/log/httpd \
	&& chown -R user1:user1 /run/httpd \
	&& chmod -R ugo+rwx /var/log/httpd \
	&& chmod -R ugo+rwx /run/httpd

EXPOSE 8080

# Dockerfile 中の設定スクリプトを抽出するスクリプトを出力、実行
COPY Dockerfile .
RUN echo $'\
cat Dockerfile | sed -n \'/^##__BEGIN/,/^##__END/p\' | sed \'s/^#//\' > startup.sh\n\
' > extract.sh && bash extract.sh

# docker-compose up の最後に実行される設定スクリプト
##__BEGIN__startup.sh__
#
#	whoami
#	echo "TZ='$TZ'"
#	[ -n "$TZ" ] && cp /usr/share/zoneinfo/$TZ /etc/localtime
#	httpd -DFOREGROUND
#	echo 'startup.sh done.'
#
##__END__startup.sh__

USER user1
ENTRYPOINT ["bash", "startup.sh"]

