#!/usr/bin/env ruby

require 'cgi'

cgi = CGI.new

now = Time.now.to_s

print(<<-HTML % now)
Content-Type: text/html

<HTML>
	<HEAD>
		<TITLE>Test</TITLE>
	</HEAD>
	<BODY>
		This is Test.
		%s now.
	</BODY>
</HTML>
HTML

open('data/rubycgi.log', 'a') {|fh|
	fh.puts('%s now.' % now)
}

__END__

